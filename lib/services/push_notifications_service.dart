// SHA1: FB:FF:B6:5A:80:DD:A1:7F:8D:52:4E:46:3D:AD:8B:03:02:64:A9:6C
import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationService {
  static FirebaseMessaging messaging = FirebaseMessaging.instance;
  static String? token;
  static StreamController<String> _messageStream = StreamController.broadcast();
  static Stream<String> get messagesStream => _messageStream.stream;

  static Future _backgroundHandler(RemoteMessage message) async {
    // print('background handler ${message.messageId}');
    _messageStream.add(message.data['product'] ?? 'No title');
  }

  static Future _onMessageHandler(RemoteMessage message) async {
    // print('onMessage handler ${message.messageId}');
    _messageStream.add(message.data['product'] ?? 'No title');
  }

  static Future _onMessageOpenAppHandler(RemoteMessage message) async {
    // print('onMessageOpenApp handler ${message.messageId}');
    _messageStream.add(message.data['product'] ?? 'No title');
  }

  static Future initializeApp() async {
    // Push notifications
    await Firebase.initializeApp();
    token = await FirebaseMessaging.instance.getToken();
    // print(token);

    // Handlers
    FirebaseMessaging.onBackgroundMessage(_backgroundHandler);
    FirebaseMessaging.onMessage.listen(_onMessageHandler);
    FirebaseMessaging.onMessageOpenedApp.listen(_onMessageOpenAppHandler);

    // Local notificacions
  }

  static closeStreams() {
    _messageStream.close();
  }
}
